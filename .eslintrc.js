module.exports = {
  root: true,
  env: {
    node: true
  },
  // extends: ["standard", "plugin:vue/recommended"],
  extends: ["standard"],
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "space-before-function-paren": 0,
    "eol-last": 0,
    semi: 0,
    quotes: 0
  },
  parserOptions: {
    parser: "babel-eslint"
  }
};
