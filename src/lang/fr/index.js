export default {
  dashboard: "Tableau de Bord",
  search: "Rechercher...",
  noResults: "Rien à afficher",
  permissions: "Permission | Permissions",
  addPermission: "Ajouter Permission",
  editPermission: "Modifier Permission",
  roles: "Rôle | Rôles",
  addRole: "Ajouter Rôle",
  editRole: "Modifier Rôle",
  name: "Nom",
  code: "Code",
  permissionCreated: "Permission créée avec succès",
  permissionNotCreated:
    "Une erreur s'est produite lors de la création de la permission. Veuillez réessayer plus tard.",
  permissionUpdated: "Permission modifiée avec succès",
  permissionNotUpdated:
    "Une erreur s'est produite lors de la mise à jour de la permission. Veuillez réessayer plus tard.",
  permissionDeleted: "Permission supprimée avec succès",
  permissionNotDeleted:
    "Une erreur s'est produite lors de la suppression de la permission. Veuillez réessayer plus tard.",
  roleCreated: "Rôle créé avec succès",
  roleNotCreated:
    "Une erreur s'est produite lors de la création du rôle. Veuillez réessayer plus tard.",
  roleUpdated: "Rôle modifié avec succès",
  roleNotUpdated:
    "Une erreur s'est produite lors de la mise à jour du rôle. Veuillez réessayer plus tard.",
  roleDeleted: "Rôle supprimé avec succès",
  roleNotDeleted:
    "Une erreur s'est produite lors de la suppression du rôle. Veuillez réessayer plus tard.",
  regions: "Région | Régions",
  addRegion: "Ajouter Région",
  editRegion: "Modifier Région",
  regionCreated: "Région créée avec succès",
  regionNotCreated:
    "Une erreur s'est produite lors de la création de la région. Veuillez réessayer plus tard.",
  regionUpdated: "Région modifiée avec succès",
  regionNotUpdated:
    "Une erreur s'est produite lors de la mise à jour de la région. Veuillez réessayer plus tard.",
  regionDeleted: "Région supprimée avec succès",
  regionNotDeleted:
    "Une erreur s'est produite lors de la suppression de la région. Veuillez réessayer plus tard.",
  departments: "Département | Départements",
  addDeparment: "Ajouter Département",
  editDepartment: "Modifier Département",
  departmentCreated: "Département créé avec succès",
  departmentNotCreated:
    "Une erreur s'est produite lors de la création du département. Veuillez réessayer plus tard.",
  departmentUpdated: "Département modifié avec succès",
  departmentNotUpdated:
    "Une erreur s'est produite lors de la mise à jour du département. Veuillez réessayer plus tard.",
  departmentDeleted: "Département supprimé avec succès",
  departmentNotDeleted:
    "Une erreur s'est produite lors de la suppression du département. Veuillez réessayer plus tard.",
  villages: "Village | Villages",
  addVillage: "Ajouter Village",
  editVillage: "Modifier Village",
  villageCreated: "Village créé avec succès",
  villageNotCreated:
    "Une erreur s'est produite lors de la création du village. Veuillez réessayer plus tard.",
  villageUpdated: "Village modifié avec succès",
  villageNotUpdated:
    "Une erreur s'est produite lors de la mise à jour du village. Veuillez réessayer plus tard.",
  villageDeleted: "Village supprimé avec succès",
  villageNotDeleted:
    "Une erreur s'est produite lors de la suppression du village. Veuillez réessayer plus tard.",
  users: "Utilisateur | Utilisateurs",
  addUser: "Ajouter Utilisateur",
  editUser: "Modifier Utilisateur",
  userCreated: "Utilisateur créé avec succès",
  userNotCreated:
    "Une erreur s'est produite lors de la création de l'utilisateur. Veuillez réessayer plus tard.",
  userUpdated: "Utilisateur modifié avec succès",
  userNotUpdated:
    "Une erreur s'est produite lors de la mise à jour de l'utilisateur. Veuillez réessayer plus tard.",
  userDeleted: "Utilisateur supprimé avec succès",
  userNotDeleted:
    "Une erreur s'est produite lors de la suppression de l'utilisateur. Veuillez réessayer plus tard.",
  firstName: "Prénom",
  lastName: "Nom",
  username: "Nom d'Utilisateur",
  phoneNumber: "Numéro de Téléphone",
  password: "Mot de Passe",
  add: "Ajouter",
  edit: "Modifier",
  delete: "Supprimer",
  submit: "Envoyer",
  cancel: "Annuler",
  close: "Close",
  login: "Connexion",
  register: "Inscription",
  logout: "Déconnexion",
  adminDashboard: "Tableau de Bord Administrateur",
  profile: "Profil",
  loggedIn: "Connecté",
  mappers: "Mappeur | Mappeurs",
  addMapper: "Ajouter Mappeur",
  editMapper: "Modifier Mappeur",
  mapperCreated: "Mappeur créé avec succès",
  mapperNotCreated:
    "Une erreur s'est produite lors de la création du mappeur. Veuillez réessayer plus tard.",
  mapperUpdated: "Mappeur modifié avec succès",
  mapperNotUpdated:
    "Une erreur s'est produite lors de la mise à jour du mappeur. Veuillez réessayer plus tard.",
  mapperDeleted: "Mappeur supprimé avec succès",
  mapperNotDeleted:
    "Une erreur s'est produite lors de la suppression du mappeur. Veuillez réessayer plus tard.",
  coaches: "Coach | Coachs",
  addCoach: "Ajouter Coach",
  editCoach: "Modifier Coach",
  coachCreated: "Coach créé avec succès",
  coachNotCreated:
    "Une erreur s'est produite lors de la création du coach. Veuillez réessayer plus tard.",
  coachUpdated: "Coach modifié avec succès",
  coachNotUpdated:
    "Une erreur s'est produite lors de la mise à jour du coach. Veuillez réessayer plus tard.",
  coachDeleted: "Coach supprimé avec succès",
  coachNotDeleted:
    "Une erreur s'est produite lors de la suppression du coach. Veuillez réessayer plus tard.",
  producers: "Producteur | Producteurs",
  addProducer: "Ajouter Producteur",
  editProducer: "Modifier Producteur",
  producerCreated: "Producteur créé avec succès",
  producerNotCreated:
    "Une erreur s'est produite lors de la création du producteur. Veuillez réessayer plus tard.",
  producerUpdated: "Producteur modifié avec succès",
  producerNotUpdated:
    "Une erreur s'est produite lors de la mise à jour du producteur. Veuillez réessayer plus tard.",
  producerDeleted: "Producteur supprimé avec succès",
  producerNotDeleted:
    "Une erreur s'est produite lors de la suppression du producteur. Veuillez réessayer plus tard.",
  createdAt: "Créé Le",
  suppliers: "Fournisseur | Fournisseurs",
  addSupplier: "Ajouter Fournisseur",
  editSupplier: "Modifier Fournisseur",
  supplierCreated: "Fournisseur créé avec succès",
  supplierNotCreated:
    "Une erreur s'est produite lors de la création du fournisseur. Veuillez réessayer plus tard.",
  supplierUpdated: "Fournisseur modifié avec succès",
  supplierNotUpdated:
    "Une erreur s'est produite lors de la mise à jour du fournisseur. Veuillez réessayer plus tard.",
  supplierDeleted: "Fournisseur supprimé avec succès",
  supplierNotDeleted:
    "Une erreur s'est produite lors de la suppression du fournisseur. Veuillez réessayer plus tard.",
  gender: "Sexe",
  male: "Male",
  female: "Female",
  certified: "Certifié",
  yes: "Oui",
  no: "Non",
  firstUtzCertification: "Première Année de Certification UTZ",
  workerNumber: "Nombre de Culitvateurs",
  internalInspectionDate: "Date d'Inspection Interne",
  deleteItem: "Supprimer l'Elément",
  confirmDelete: "Voulez-vous vraiment supprimer '{item}'?",
  avatar: "Avatar",
  currentYearProduction: "Production Estimée de l'Année Actuelle",
  previousYearProduction: "Production de l'Année Précédente",
  certifiedCropArea: "Surface de la Plantation Certifiée",
  plotNumber: "Nombre de Champs",
  previousYearVolumeDelivered: "Volume Total Livré l'Année Précédente",
  producerNumber: "Nombre de Producteurs",
  editProfile: "Modifier Profil",
  completeProfile: "Compléter votre profil",
  updateProfile: "Mettre Profil à Jour",
  profileDescription: "Il ne faudrait pas craindre la vérité car nous avons le devoir de reprendre la fondation humaine dans la vérité, et ceci ne peut être accompli que par la contribution de chaque unité distincte...peu importe leur taille.",
  viewDetailedProfile: "Afficher profil détaillé"
};
