import { set } from "@/utils/vuex";

export default {
  setRegion: set("region"),
  setRegions: set("regions")
};
