import { set } from "@/utils/vuex";

export default {
  setDepartment: set("department"),
  setDepartments: set("departments")
};
