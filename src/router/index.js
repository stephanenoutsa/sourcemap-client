/**
 * Vue Router
 *
 * @library
 *
 * https://router.vuejs.org/en/
 */

// Lib imports
import Vue from "vue";
import VueAnalytics from "vue-analytics";
import Router from "vue-router";
import Meta from "vue-meta";

// Routes
import paths from "./paths";

function route(path, view, name, children, component, redirect, meta, alias) {
  let route = {};

  if (children && children.length) {
    route = {
      path,
      redirect,
      component: resolve => import(/* webpackChunkName: "[request]" */ `@/layouts/${component}.vue`).then(resolve),
      children: children.map(c => {
        if (c.children && c.children.length) {
          c.path = c.path;
          c.redirect = c.redirect;
          c.component = resolve => import(/* webpackChunkName: "[request]" */ `@/layouts/${c.layout}.vue`).then(resolve);
          c.children = c.children.map(child => {
            child.name = child.name || child.view;
            child.path = child.path;
            child.component = resolve => import(/* webpackChunkName: "[request]" */ `@/views/${child.view}.vue`).then(resolve);
            child.meta = child.meta;

            return child;
          });
        } else {
          c.name = c.name || c.view;
          c.path = c.path;
          c.component = resolve => import(/* webpackChunkName: "[request]" */ `@/views/${c.view}.vue`).then(resolve);
          c.meta = c.meta;
        }

        return c;
      })
    };

    if (alias) {
      route.alias = alias;
    }
  } else {
    route = {
      name: name || view,
      path,
      component: resolve => import(/* webpackChunkName: "[request]" */ `@/views/${view}.vue`).then(resolve),
      meta
    };
  }

  return route;
}

Vue.use(Router);

const routes = paths.map(path => route(
  path.path, path.view, path.name, path.children, path.component,
  path.redirect, path.meta, path.alias
)).concat([{ path: "*", redirect: "/" }]);


// Create a new router
const router = new Router({
  mode: "history",
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    if (to.hash) {
      return { selector: to.hash };
    }
    return { x: 0, y: 0 };
  }
});

Vue.use(Meta);

// Bootstrap Analytics
// Set in .env
// https://github.com/MatteoGabriele/vue-analytics
if (process.env.GOOGLE_ANALYTICS) {
  Vue.use(VueAnalytics, {
    id: process.env.GOOGLE_ANALYTICS,
    router,
    autoTracking: {
      page: process.env.NODE_ENV !== "development"
    }
  });
}

export default router;
