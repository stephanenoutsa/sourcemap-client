import { set } from "@/utils/vuex";

export default {
  setCoach: set("coach"),
  setCoaches: set("coaches")
};
