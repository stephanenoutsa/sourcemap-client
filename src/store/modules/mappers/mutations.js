import { set } from "@/utils/vuex";

export default {
  setMapper: set("mapper"),
  setMappers: set("mappers")
};
