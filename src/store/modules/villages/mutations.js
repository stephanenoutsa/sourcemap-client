import { set } from "@/utils/vuex";

export default {
  setVillage: set("village"),
  setVillages: set("villages")
};
