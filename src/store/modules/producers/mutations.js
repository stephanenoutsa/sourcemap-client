import { set } from "@/utils/vuex";

export default {
  setProducer: set("producer"),
  setProducers: set("producers")
};
