// Private variables
const { app, BrowserWindow } = require("electron");
const mUrl = require("url");
const path = require("path");

// const url =
//   process.env.NODE_ENV === "DEV"
//     ? "http://localhost:8080/"
//     : `file://${process.cwd()}/dist/index.html`;

let url;

if (process.env.NODE_ENV === "DEV") {
  url = "http://localhost:8080/";
} else {
  url = mUrl.format({
    pathname: path.join(__dirname, "..", "dist/index.html"),
    protocol: "file:",
    slashes: true
  });
}

let window;

// Initialize the window to our specified dimensions
app.on("ready", () => {
  window = new BrowserWindow({
    width: 1080,
    minWidth: 680,
    height: 840,
    title: app.name,
    webPreferences: { nodeIntegration: true }
  });

  // Open developer tools
  // window.webContents.openDevTools();

  // Specify entry point to default entry point of vue.js
  window.loadURL(url);

  // Remove window once app is closed
  window.on("closed", function() {
    window = null;
  });
});

// Create the application window if the window variable is null
app.on("activate", () => {
  if (window === null) {
    createWindow();
  }
});

// Quit the app once closed
app.on("window-all-closed", function() {
  if (process.platform !== "darwin") {
    app.quit();
  }
});
