/**
 * App Configurations
 */
export default {
  brandName: "Tield Force",
  baseUrl:
    process.env.NODE_ENV === "development"
      ? "http://localhost:8080"
      : "http://localhost:8080",
  // : "https://" + window.location.hostname,
  backUrl:
    process.env.NODE_ENV === "development"
      ? "http://localhost:1000"
      : "http://localhost:1000",
  apiUrl:
    process.env.NODE_ENV === "development"
      ? "http://localhost:1000/api/v0"
      : "http://localhost:1000/api/v0",
  // : "https://" + window.location.hostname + "/api/v0",
  brandLogo: "/images/logo.png",
  favicon: "/favicon.ico",
  facebookClientID: "592464221221543",
  facebookClientSecret: "09a2ec233e21b5fa8160e54ce70470f4",
  googleClientID:
    "617525972264-k22ne2ees5qmhklhrnbgfvvfginvu81n.apps.googleusercontent.com",
  googleClientSecret: "EHDXs-To92ZbuTC42arW_6p7"
};
