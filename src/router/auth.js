/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */

import i18n from "@/plugins/i18n";

export default {
  path: '/auth',
  name: 'auth',
  component: "AuthLayout",
  redirect: '/login',
  children: [
    {
      path: '/login',
      name: i18n.t('message.login'),
      view: "Auth/Login",
      meta: {
        guest: true
      }
    }
    /* {
      path: '/register',
      name: i18n.t('message.register'),
      component: "Auth/Register",
      meta: {
        guest: true
      }
    } */
  ]
}
