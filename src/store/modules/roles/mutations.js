import { set } from "@/utils/vuex";

export default {
  setRole: set("role"),
  setRoles: set("roles")
};
