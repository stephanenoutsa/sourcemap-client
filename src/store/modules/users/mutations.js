import { set } from "@/utils/vuex";

export default {
  setUser: set("user"),
  setUsers: set("users")
};
