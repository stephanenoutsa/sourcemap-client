const isProd = process.env.NODE_ENV === "production";

module.exports = {
  publicPath: isProd ? `${process.cwd()}/dist/` : "/",
  devServer: {
    disableHostCheck: true
  }
};
