// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";

// Components
import "./components";

// Plugins
import "./plugins";
import { sync } from "vuex-router-sync";
import Vuelidate from "vuelidate";
import Notifications from "vue-notification";
import velocity from "velocity-animate";
import axios from "axios";
import VueAxios from "vue-axios";

// Vue i18n
import i18n from "./plugins/i18n";

// Application imports
import App from "./App";
import router from "@/router";
import store from "@/store";

// Sync store with router
sync(store, router);

Vue.use(Vuelidate);
Vue.use(Notifications, { velocity });
Vue.use(VueAxios, axios);

Vue.config.productionTip = false;

// Navigation guards before each request
router.beforeResolve((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // User needs to be logged in
    if (localStorage.getItem("user") === null) {
      next({
        path: "/login",
        query: { redirect: to.fullPath }
      });
    } else {
      const date = localStorage.getItem("token-expires-at");

      const expiryDate = new Date(date);
      const today = new Date();

      // console.log("Diff:", today, date, expiryDate, today > expiryDate);

      if (today > expiryDate) {
        // If token has expired, logout user
        localStorage.removeItem('user');
        localStorage.removeItem('tf-token');
        localStorage.removeItem('token-expires-at');

        next({
          path: "/login",
          query: { redirect: to.fullPath }
        });
      } else if (to.matched.some(record => record.meta.requiresAdmin)) {
        // Needs to be admin
        const role = JSON.parse(localStorage.getItem('user')).role.code;

        if (role === 'coordinator') {
          next();
        } else {
          next({
            path: "/dashboard"
          });
        }
      }

      next();
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (localStorage.getItem("tf-token") == null) {
      next();
    } else {
      next({ path: "/dashboard" });
    }
  } else {
    // User doesn't need to be logged in
    next();
  }
});

// Navigation guards after each request
router.afterEach((to, from) => {
  // Complete the animation of the route progress bar.
});

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
