import AppConfig from "@/constants/AppConfig";
import Vue from "vue";

const apiURL = AppConfig.apiUrl;

export const apiModel = {
  Auth: {
    Login: {
      method: "POST",
      url: "/auth/login"
    }
  },

  Permission: {
    List: {
      method: "GET",
      url: "/permissions"
    },
    Get: {
      method: "GET",
      url: "/permissions/:permission"
    },
    Create: {
      method: "POST",
      url: "/permissions"
    },
    Update: {
      method: "PUT",
      url: "/permissions/:permission"
    },
    Delete: {
      method: "DELETE",
      url: "/permissions/:permission"
    }
  },

  Role: {
    List: {
      method: "GET",
      url: "/roles"
    },
    Get: {
      method: "GET",
      url: "/roles/:role"
    },
    Create: {
      method: "POST",
      url: "/roles"
    },
    Update: {
      method: "PUT",
      url: "/roles/:role"
    },
    Delete: {
      method: "DELETE",
      url: "/roles/:role"
    }
  },

  Region: {
    List: {
      method: "GET",
      url: "/regions"
    },
    Get: {
      method: "GET",
      url: "/regions/:region"
    },
    Create: {
      method: "POST",
      url: "/regions"
    },
    Update: {
      method: "PUT",
      url: "/regions/:region"
    },
    Delete: {
      method: "DELETE",
      url: "/regions/:region"
    }
  },

  Department: {
    List: {
      method: "GET",
      url: "/departments"
    },
    Get: {
      method: "GET",
      url: "/departments/:department"
    },
    Create: {
      method: "POST",
      url: "/departments"
    },
    Update: {
      method: "PUT",
      url: "/departments/:department"
    },
    Delete: {
      method: "DELETE",
      url: "/departments/:department"
    }
  },

  Village: {
    List: {
      method: "GET",
      url: "/villages"
    },
    Get: {
      method: "GET",
      url: "/villages/:village"
    },
    Create: {
      method: "POST",
      url: "/villages"
    },
    Update: {
      method: "PUT",
      url: "/villages/:village"
    },
    Delete: {
      method: "DELETE",
      url: "/villages/:village"
    }
  },

  User: {
    List: {
      method: "GET",
      url: "/users"
    },
    Get: {
      method: "GET",
      url: "/users/:user"
    },
    Create: {
      method: "POST",
      url: "/users"
    },
    Update: {
      method: "PUT",
      url: "/users/:user"
    },
    Delete: {
      method: "DELETE",
      url: "/users/:user"
    }
  },

  Supplier: {
    List: {
      method: "GET",
      url: "/suppliers"
    },
    Get: {
      method: "GET",
      url: "/suppliers/:supplier"
    },
    Create: {
      method: "POST",
      url: "/suppliers"
    },
    Update: {
      method: "PUT",
      url: "/suppliers/:supplier"
    },
    Delete: {
      method: "DELETE",
      url: "/suppliers/:supplier"
    }
  },

  Producer: {
    List: {
      method: "GET",
      url: "/producers"
    },
    Get: {
      method: "GET",
      url: "/producers/:producer"
    },
    Create: {
      method: "POST",
      url: "/producers"
    },
    Update: {
      method: "PUT",
      url: "/producers/:producer"
    },
    Delete: {
      method: "DELETE",
      url: "/producers/:producer"
    }
  },

  Mapper: {
    List: {
      method: "GET",
      url: "/mappers"
    },
    Get: {
      method: "GET",
      url: "/mappers/:mapper"
    },
    Create: {
      method: "POST",
      url: "/mappers"
    },
    Update: {
      method: "PUT",
      url: "/mappers/:mapper"
    },
    Delete: {
      method: "DELETE",
      url: "/mappers/:mapper"
    }
  },

  Coach: {
    List: {
      method: "GET",
      url: "/coaches"
    },
    Get: {
      method: "GET",
      url: "/coaches/:coach"
    },
    Create: {
      method: "POST",
      url: "/coaches"
    },
    Update: {
      method: "PUT",
      url: "/coaches/:coach"
    },
    Delete: {
      method: "DELETE",
      url: "/coaches/:coach"
    }
  }
};

export const makeApiRequest = async (
  args,
  object,
  token = "",
  errorMessages = {},
  queryArgs = {},
  customHeaders = {}
) => {
  const method = object.method;
  let urlSuffix = object.url;
  let finalUrlSuffix = "";

  // Adjust API url
  if (Object.keys(args).length === 0 && args.constructor === Object) {
    finalUrlSuffix = urlSuffix;
  } else if (args instanceof FormData) {
    for (let pair of args.entries()) {
      finalUrlSuffix = urlSuffix.replace(`:${pair[0]}`, pair[1]);
      urlSuffix = finalUrlSuffix;
    }
  } else {
    Object.keys(args).forEach(key => {
      finalUrlSuffix = urlSuffix.replace(`:${key}`, args[key]);
      urlSuffix = finalUrlSuffix;
    });
  }

  // Set query parameters
  if (!(Object.keys(queryArgs).length === 0 && queryArgs.constructor === Object)) {
    let suffix = '?';
    let i = 0;

    Object.keys(queryArgs).forEach(key => {
      suffix += i ? '&' : '';
      suffix += `${key}=${queryArgs[key]}`;

      i++;
    });

    finalUrlSuffix += suffix;
  }

  // Set headers
  customHeaders["headers"] = customHeaders["headers"] ? customHeaders["headers"] : {};

  // Set token in request header if necessary
  if (token !== "") {
    customHeaders["headers"]["Authorization"] = "Bearer " + token;
  }

  let response;

  switch (method) {
    case "POST":
      response = await Vue.axios
        .post(apiURL + finalUrlSuffix.trim(), args, customHeaders);

      return response;

    case "GET":
      response = await Vue.axios
        .get(apiURL + finalUrlSuffix.trim(), {
          params: args,
          headers: customHeaders["headers"]
        });

      return response;

    case "PUT":
      response = await Vue.axios
        .put(apiURL + finalUrlSuffix.trim(), args, customHeaders);

      return response;

    case "DELETE":
      response = await Vue.axios
        .delete(apiURL + finalUrlSuffix.trim(), customHeaders);

      return response;
  }
};
