import { set } from "@/utils/vuex";

export default {
  setPermission: set("permission"),
  setPermissions: set("permissions")
};
