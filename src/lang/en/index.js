export default {
  dashboard: "Dashboard",
  search: "Search...",
  noResults: "Nothing to display",
  permissions: "Permission | Permissions",
  addPermission: "Add Permission",
  editPermission: "Edit Permission",
  roles: "Role | Roles",
  addRole: "Add Role",
  editRole: "Edit Role",
  name: "Name",
  code: "Code",
  permissionCreated: "Permission created successfully",
  permissionNotCreated:
    "An error occurred when creating the permission. Please try again later.",
  permissionUpdated: "Permission updated successfully",
  permissionNotUpdated:
    "An error occurred when updating the permission. Please try again later.",
  permissionDeleted: "Permission deleted successfully",
  permissionNotDeleted:
    "An error occurred when deleting the permission. Please try again later.",
  roleCreated: "Role created successfully",
  roleNotCreated:
    "An error occurred when creating the role. Please try again later.",
  roleUpdated: "Role updated successfully",
  roleNotUpdated:
    "An error occurred when updating the role. Please try again later.",
  roleDeleted: "Role deleted successfully",
  roleNotDeleted:
    "An error occurred when deleting the role. Please try again later.",
  regions: "Region | Regions",
  addRegion: "Add Region",
  editRegion: "Edit Region",
  regionCreated: "Region created successfully",
  regionNotCreated:
    "An error occurred when creating the region. Please try again later.",
  regionUpdated: "Region updated successfully",
  regionNotUpdated:
    "An error occurred when updating the region. Please try again later.",
  regionDeleted: "Region deleted successfully",
  regionNotDeleted:
    "An error occurred when deleting the region. Please try again later.",
  departments: "Department | Departments",
  addDepartment: "Add Department",
  editDepartment: "Edit Department",
  departmentCreated: "Department created successfully",
  departmentNotCreated:
    "An error occurred when creating the department. Please try again later.",
  departmentUpdated: "Department updated successfully",
  departmentNotUpdated:
    "An error occurred when updating the department. Please try again later.",
  departmentDeleted: "Department deleted successfully",
  departmentNotDeleted:
    "An error occurred when deleting the department. Please try again later.",
  villages: "Village | Villages",
  addVillage: "Add Village",
  editVillage: "Edit Village",
  villageCreated: "Village created successfully",
  villageNotCreated:
    "An error occurred when creating the village. Please try again later.",
  villageUpdated: "Village updated successfully",
  villageNotUpdated:
    "An error occurred when updating the village. Please try again later.",
  villageDeleted: "Village deleted successfully",
  villageNotDeleted:
    "An error occurred when deleting the village. Please try again later.",
  users: "User | Users",
  addUser: "Add User",
  editUser: "Edit User",
  userCreated: "User created successfully",
  userNotCreated:
    "An error occurred when creating the user. Please try again later.",
  userUpdated: "User updated successfully",
  userNotUpdated:
    "An error occurred when updating the user. Please try again later.",
  userDeleted: "User deleted successfully",
  userNotDeleted:
    "An error occurred when deleting the user. Please try again later.",
  firstName: "First Name",
  lastName: "Last Name",
  username: "Username",
  phoneNumber: "Phone Number",
  password: "Password",
  add: "Add",
  edit: "Edit",
  delete: "Delete",
  submit: "Submit",
  cancel: "Cancel",
  close: "Close",
  login: "Login",
  register: "Register",
  logout: "Logout",
  adminDashboard: "Admin Dashboard",
  profile: "Profile",
  loggedIn: "Logged in",
  mappers: "Mapper | Mappers",
  addMapper: "Add Mapper",
  editMapper: "Edit Mapper",
  mapperCreated: "Mapper created successfully",
  mapperNotCreated:
    "An error occurred when creating the mapper. Please try again later.",
  mapperUpdated: "Mapper updated successfully",
  mapperNotUpdated:
    "An error occurred when updating the mapper. Please try again later.",
  mapperDeleted: "Mapper deleted successfully",
  mapperNotDeleted:
    "An error occurred when deleting the mapper. Please try again later.",
  coaches: "Coach | Coaches",
  addCoach: "Add Coach",
  editCoach: "Edit Coach",
  coachCreated: "Coach created successfully",
  coachNotCreated:
    "An error occurred when creating the coach. Please try again later.",
  coachUpdated: "Coach updated successfully",
  coachNotUpdated:
    "An error occurred when updating the coach. Please try again later.",
  coachDeleted: "Coach deleted successfully",
  coachNotDeleted:
    "An error occurred when deleting the coach. Please try again later.",
  producers: "Producer | Producers",
  addProducer: "Add Producer",
  editProducer: "Edit Producer",
  producerCreated: "Producer created successfully",
  producerNotCreated:
    "An error occurred when creating the producer. Please try again later.",
  producerUpdated: "Producer updated successfully",
  producerNotUpdated:
    "An error occurred when updating the producer. Please try again later.",
  producerDeleted: "Producer deleted successfully",
  producerNotDeleted:
    "An error occurred when deleting the producer. Please try again later.",
  createdAt: "Created At",
  suppliers: "Supplier | Suppliers",
  addSupplier: "Add Supplier",
  editSupplier: "Edit Supplier",
  supplierCreated: "Supplier created successfully",
  supplierNotCreated:
    "An error occurred when creating the supplier. Please try again later.",
  supplierUpdated: "Supplier updated successfully",
  supplierNotUpdated:
    "An error occurred when updating the supplier. Please try again later.",
  supplierDeleted: "Supplier deleted successfully",
  supplierNotDeleted:
    "An error occurred when deleting the supplier. Please try again later.",
  gender: "Gender",
  male: "Male",
  female: "Female",
  certified: "Certified",
  yes: "Yes",
  no: "No",
  firstUtzCertification: "First Year of UTZ Certification",
  workerNumber: "Number of Workers",
  internalInspectionDate: "Internal Inspection Date",
  deleteItem: "Delete Item",
  confirmDelete: "Are you sure you want to delete '{item}'?",
  avatar: "Avatar",
  currentYearProduction: "Estimated Production of the Current Year",
  previousYearProduction: "Previous Year's Production",
  certifiedCropArea: "Total Surface Area of Certified Crop",
  plotNumber: "Number of Plots",
  previousYearVolumeDelivered: "Total Volume Delivered from the Previous Year",
  producerNumber: "Number of Producers",
  editProfile: "Edit Profile",
  completeProfile: "Complete your profile",
  updateProfile: "Update Profile",
  profileDescription: "Don't be scared of the truth because we need to restart the human foundation in truth, and this can only be done with the contribution of each distinct unit...however small they may seem to be.",
  viewDetailedProfile: "View detailed profile"
};
