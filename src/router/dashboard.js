/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */

import i18n from "@/plugins/i18n";

export default {
  path: "/dashboard",
  alias: "/",
  component: "DashboardLayout",
  redirect: i18n.t('message.dashboard'),
  children: [
    {
      path: "",
      name: i18n.t("message.dashboard"),
      // Relative to /src/views
      view: "Dashboard",
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "permissions",
      redirect: i18n.tc("message.permissions", 2),
      layout: "InheritLayout",
      children: [
        {
          path: "",
          name: i18n.tc("message.permissions", 2),
          view: "Dashboard/Permissions/Permissions",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        },
        {
          path: "add",
          name: i18n.t("message.addPermission"),
          view: "Dashboard/Permissions/AddPermission",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        },
        {
          path: ":permission/edit",
          name: i18n.t("message.editPermission"),
          view: "Dashboard/Permissions/EditPermission",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        }
      ]
    },
    {
      path: "roles",
      redirect: i18n.tc("message.roles", 2),
      layout: "InheritLayout",
      children: [
        {
          path: "",
          name: i18n.tc("message.roles", 2),
          view: "Dashboard/Roles/Roles",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        },
        {
          path: "add",
          name: i18n.t("message.addRole"),
          view: "Dashboard/Roles/AddRole",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        },
        {
          path: ":role/edit",
          name: i18n.t("message.editRole"),
          view: "Dashboard/Roles/EditRole",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        }
      ]
    },
    {
      path: "regions",
      redirect: i18n.tc("message.regions", 2),
      layout: "InheritLayout",
      children: [
        {
          path: "",
          name: i18n.tc("message.regions", 2),
          view: "Dashboard/Regions/Regions",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        },
        {
          path: "add",
          name: i18n.t("message.addRegion"),
          view: "Dashboard/Regions/AddRegion",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        },
        {
          path: ":region/edit",
          name: i18n.t("message.editRegion"),
          view: "Dashboard/Regions/EditRegion",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        }
      ]
    },
    {
      path: "departments",
      redirect: i18n.tc("message.departments", 2),
      layout: "InheritLayout",
      children: [
        {
          path: "",
          name: i18n.tc("message.departments", 2),
          view: "Dashboard/Departments/Departments",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        },
        {
          path: "add",
          name: i18n.t("message.addDepartment"),
          view: "Dashboard/Departments/AddDepartment",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        },
        {
          path: ":department/edit",
          name: i18n.t("message.editDepartment"),
          view: "Dashboard/Departments/EditDepartment",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        }
      ]
    },
    {
      path: "villages",
      redirect: i18n.tc("message.villages", 2),
      layout: "InheritLayout",
      children: [
        {
          path: "",
          name: i18n.tc("message.villages", 2),
          view: "Dashboard/Villages/Villages",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        },
        {
          path: "add",
          name: i18n.t("message.addVillage"),
          view: "Dashboard/Villages/AddVillage",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        },
        {
          path: ":village/edit",
          name: i18n.t("message.editVillage"),
          view: "Dashboard/Villages/EditVillage",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        }
      ]
    },
    {
      path: "users",
      redirect: i18n.tc("message.users", 2),
      layout: "InheritLayout",
      children: [
        {
          path: "",
          name: i18n.tc("message.users", 2),
          view: "Dashboard/Users/Users",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        },
        {
          path: "add",
          name: i18n.t("message.addUser"),
          view: "Dashboard/Users/AddUser",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        },
        {
          path: ":user/edit",
          name: i18n.t("message.editUser"),
          view: "Dashboard/Users/EditUser",
          meta: {
            requiresAuth: true,
            requiresAdmin: true
          }
        }
      ]
    },
    {
      path: "suppliers",
      redirect: i18n.tc("message.suppliers", 2),
      layout: "InheritLayout",
      children: [
        {
          path: "",
          name: i18n.tc("message.suppliers", 2),
          view: "Dashboard/Suppliers/Suppliers",
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "add",
          name: i18n.t("message.addSupplier"),
          view: "Dashboard/Suppliers/AddSupplier",
          meta: {
            requiresAuth: true
          }
        },
        {
          path: ":supplier",
          name: i18n.tc("message.suppliers", 1),
          view: "Dashboard/Suppliers/Supplier",
          meta: {
            requiresAuth: true
          }
        },
        {
          path: ":supplier/edit",
          name: i18n.t("message.editSupplier"),
          view: "Dashboard/Suppliers/EditSupplier",
          meta: {
            requiresAuth: true
          }
        }
      ]
    },
    {
      path: "producers",
      redirect: i18n.tc("message.producers", 2),
      layout: "InheritLayout",
      children: [
        {
          path: "",
          name: i18n.tc("message.producers", 2),
          view: "Dashboard/Producers/Producers",
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "add",
          name: i18n.t("message.addProducer"),
          view: "Dashboard/Producers/AddProducer",
          meta: {
            requiresAuth: true
          }
        },
        {
          path: ":producer",
          name: i18n.tc("message.producers", 1),
          view: "Dashboard/Producers/Producer",
          meta: {
            requiresAuth: true
          }
        },
        {
          path: ":producer/edit",
          name: i18n.t("message.editProducer"),
          view: "Dashboard/Producers/EditProducer",
          meta: {
            requiresAuth: true
          }
        }
      ]
    },
    {
      path: "mappers",
      redirect: i18n.tc("message.mappers", 2),
      layout: "InheritLayout",
      children: [
        {
          path: "",
          name: i18n.tc("message.mappers", 2),
          view: "Dashboard/Mappers/Mappers",
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "add",
          name: i18n.t("message.addMapper"),
          view: "Dashboard/Mappers/AddMapper",
          meta: {
            requiresAuth: true
          }
        },
        {
          path: ":mapper/edit",
          name: i18n.t("message.editMapper"),
          view: "Dashboard/Mappers/EditMapper",
          meta: {
            requiresAuth: true
          }
        }
      ]
    },
    {
      path: "coaches",
      redirect: i18n.tc("message.coaches", 2),
      layout: "InheritLayout",
      children: [
        {
          path: "",
          name: i18n.tc("message.coaches", 2),
          view: "Dashboard/Coaches/Coaches",
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "add",
          name: i18n.t("message.addCoach"),
          view: "Dashboard/Coaches/AddCoach",
          meta: {
            requiresAuth: true
          }
        },
        {
          path: ":coach/edit",
          name: i18n.t("message.editCoach"),
          view: "Dashboard/Coaches/EditCoach",
          meta: {
            requiresAuth: true
          }
        }
      ]
    },
    {
      path: "user-profile",
      name: i18n.t("message.profile"),
      view: "Dashboard/UserProfile",
      meta: {
        requiresAuth: true
      }
    }
  ]
}
