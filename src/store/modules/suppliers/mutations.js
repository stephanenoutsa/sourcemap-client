import { set } from "@/utils/vuex";

export default {
  setSupplier: set("supplier"),
  setSuppliers: set("suppliers")
};
