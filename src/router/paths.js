/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */

import authRoutes from "./auth";
import dashboardRoutes from "./dashboard";

export default [
  authRoutes,
  dashboardRoutes,
  {
    path: "/table-list",
    name: "Table List",
    view: "TableList"
  },
  {
    path: "/typography",
    view: "Typography"
  },
  {
    path: "/icons",
    view: "Icons"
  },
  {
    path: "/maps",
    view: "Maps"
  },
  {
    path: "/notifications",
    view: "Notifications"
  },
  {
    path: "/upgrade",
    name: "Upgrade to PRO",
    view: "Upgrade"
  }
];
